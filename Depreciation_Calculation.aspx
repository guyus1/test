<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Depreciation_Calculation.aspx.vb" Inherits="Depreciation_Calculation" title="Fixed Assets Depreciation" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <span style="color: #333399; text-align: center">
        <table style="font-weight: bold; width: 100%; height: 100%">
            <tr>
                <td colspan="5" style="text-align: center">
                    <asp:Label ID="Label1" runat="server" BackColor="LightSteelBlue" Font-Bold="True"
                        Font-Size="Medium" ForeColor="#333399" Text="Depreciation Calculation" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 25%; text-align: right;" colspan="2">
                    CCN :</td>
                <td colspan="3" style="width: 75%; text-align: left">
                    <asp:DropDownList ID="ddl_ccn" runat="server" AutoPostBack="True"
                        Width="333px">
                    </asp:DropDownList>
                    <asp:Label ID="lblAsOfDate" runat="server" Width="80px"></asp:Label>
                    <asp:Label ID="lblCCN" runat="server" Width="80px"></asp:Label>
                    <asp:Label ID="lblyear" runat="server" Visible="False"></asp:Label>
                    <asp:TextBox ID="txtasofdate" runat="server" Visible="False" Width="80px"></asp:TextBox>
                    <asp:Label ID="lblddmmyy_txt" runat="server" Text="ddmmyy (Overriden)" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 25%; text-align: right;" colspan="2">
                    Option :</td>
                <td colspan="3" style="width: 75%; text-align: left">
                    <asp:RadioButtonList ID="rblProcId" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Value="C" Selected="True">Calculation</asp:ListItem>
                        <asp:ListItem Value="I">Inquiry</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <tr>
                <td style="width: 25%; height: 18px; text-align: right;" colspan="2">
                    Data Type :</td>
                <td colspan="3" style="height: 18px; width: 75%; text-align: left;">
                            <asp:CheckBoxList ID="cblCalcType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                                <asp:ListItem Value="N">New Purchase</asp:ListItem>
                                <asp:ListItem Value="S">Disposal</asp:ListItem>
                                <asp:ListItem Value="E">Add Expense</asp:ListItem>
                                <asp:ListItem Value="T">Transfer</asp:ListItem>
                                <asp:ListItem Value="M">M/T Asset</asp:ListItem>
                                <asp:ListItem Value="R">Recalculate</asp:ListItem>
                                <asp:ListItem Value="RX">Recovery</asp:ListItem>
                            </asp:CheckBoxList>
                    <asp:TextBox ID="txtselasset" runat="server" Visible="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center; height: 26px;">
                            <asp:Button ID="btnOK" runat="server" Text="View Data" />
                            <asp:Button ID="btnCalc" runat="server" Text="Calculate" OnClientClick="return confirm('Are you sure you want to Calculate Depre?');" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" /></td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:MultiView ID="MVDepre" runat="server">
                        <asp:View ID="vCalc" runat="server">
                            <asp:GridView ID="gvAsset" runat="server" CellPadding="4" ForeColor="#333333" AutoGenerateColumns="False" ShowFooter="True" GridLines="None" HorizontalAlign="Center" Font-Bold="False">
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <EditRowStyle BackColor="#999999" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="cbSelAll" runat="server" AutoPostBack="True" OnCheckedChanged="cbSelAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbCalc" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CCN" Visible="False" />
                                    <asp:TemplateField HeaderText="Asset No">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblAssetNO" runat="server" Text='<%# Bind("ASSET_NO") %>' Width="100%"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAssetNO" runat="server" Text='<%# Bind("ASSET_NO") %>' Width="100%"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="old_asset_no" HeaderText="Old Asset" />
                                    <asp:BoundField DataField="asset_description" HeaderText="Description">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Asset_type" HeaderText="Type">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Product_code" HeaderText="Product">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Age" HeaderText="Age">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Start_date" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Start Date"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="End_Date" DataFormatString="{0:dd/MM/yyyy}" HeaderText="End Date"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="netbv" DataFormatString="{0:N2}" HeaderText="Acquisition Cost"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="inv_qty" HeaderText="InvQty" />
                                    <asp:BoundField DataField="depre_method" HeaderText="DP Meth">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Asset STS">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblDatatype" runat="server" Text='<%# Bind("type") %>' Width="100%"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDatatype" runat="server" Text='<%# Bind("type") %>' Width="100%"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Adjust cost">
                                        <EditItemTemplate>
                                            <asp:Label ID="lbladjCost" runat="server" Text='<%# Bind("adj_cost", "{0:n2}") %>'
                                                Width="100%"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbladjCost" runat="server" Text='<%# Bind("adj_cost", "{0:n2}") %>'
                                                Width="100%"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                   
                                </Columns>
                                <EmptyDataTemplate>
                                    !!! No Data Found !!!
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </asp:View>
                        <asp:View ID="vInquiry" runat="server">
                            <asp:GridView ID="gvDepre_TypeSub" runat="server" HorizontalAlign="Center" CellPadding="4" ForeColor="#333333" ShowFooter="True" AutoGenerateColumns="False" Font-Bold="False">
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <EditRowStyle BackColor="#999999" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <EmptyDataTemplate>
                                    !!! No Data Found !!!
                                </EmptyDataTemplate>
                                <Columns>
                                <asp:BoundField HeaderText="Type" DataField="Type" />
                                    <asp:BoundField DataField="product_code" HeaderText="Product" />
                                    <asp:BoundField DataField="dp_src" HeaderText="DP Source">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                <asp:BoundField HeaderText="MTD_Depre" DataField="mtd_depre" DataFormatString="{0:N2}"  >
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="YTD_Depre" DataField="ytd_depre" DataFormatString="{0:N2}"  >
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </asp:View>
                    </asp:MultiView></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
    </span>
</asp:Content>

