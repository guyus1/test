Imports System
Imports System.Data
Imports System.Data.OracleClient
Imports CommonFN
Partial Class Depreciation_Calculation
    Inherits System.Web.UI.Page
    Dim mySelectQuery As String
    Dim myOrderQuery As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Session("userid") = "" Then
            Else
                CommonFN.CCN_show(ddl_ccn, Session())
            End If
        End If
        'If Not Page.IsPostBack Then
        '    cblCalcType.Visible = False
        '    btnOK.Visible = False
        '    btnCalc.Visible = False
        '    btnCancel.Visible = False

        'End If


    End Sub

    Protected Sub rblProcId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblProcId.SelectedIndexChanged
        MVDepre.Visible = False
        If lblCCN.Text = "" Then
            MessageBox("Please select CCN to work with !!!")

        Else
            cblCalcType.Visible = True
            btnOK.Visible = True
            btnCancel.Visible = True

            If rblProcId.SelectedValue = "C" Then
                btnCalc.Visible = True
            Else
                btnCalc.Visible = False
            End If
            '    MVDepre.SetActiveView(vInquiry)
            '    'Bind data
            '    myConnection = New Data.OracleClient.OracleConnection(ConnFA)
            '    myConnection.Open()
            '    mySelectQuery = "select b.asset_type as Type,b.asset_subtype as SubType,TO_CHAR(sum(decode(a.month||a.year," & mmyyyy & ",a.amount,0)),'999,999,999,999.99') as mth_depre,TO_CHAR(sum(decode(a.month||a.year,'" & mmyyyy & "',0,a.amount)),'999,999,999,999.99') as ytd_depre from cfa_depre a, cfa_asset b where a.ASSET_NO = b.asset_no and b.ccn = '" & lblCCN.Text & "' /*and b.status = 'A' and b.STATUS = 'P'*/ group by b.ccn,b.asset_type,b.asset_subtype"
            '    ' Response.Write(mySelectQuery)
            '    da = New Data.OracleClient.OracleDataAdapter(mySelectQuery, myConnection)
            '    ds = New Data.DataSet
            '    da.Fill(ds, "depre")
            '    gvDepre_TypeSub.DataSource = ds.Tables("depre")
            '    gvDepre_TypeSub.DataBind()
            '    myConnection.Close()

            'End If
            'If rblProcId.SelectedValue = "C" Then
            '    MVDepre.SetActiveView(vCalc)
            'End If
        End If
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Session("userid") <> "" Then
            If lblCCN.Text = "" Then
                MessageBox("Please select CCN to work with !!!")
            Else
                Dim mmyyyy As String = Mid(lblAsOfDate.Text, 4, 2) & Mid(lblAsOfDate.Text, 7, 4)

                If Session("userid") <> "" Then
                    MVDepre.Visible = True

                    If rblProcId.SelectedValue = "C" Then
                        MVDepre.SetActiveView(vCalc)
                        showAsset()
                    Else
                        MVDepre.SetActiveView(vInquiry)
                        showInquiry()
                    End If
                End If
            End If
        End If
    End Sub
    
    Sub showInquiry()


        Dim DBcon As New DbConnect(Session("dbsrcName"))

        Dim mmyyyy As String = Mid(lblAsOfDate.Text, 4, 2) & Mid(lblAsOfDate.Text, 7, 4)
        Dim selChkbox As String = [String].Empty
        For i As Integer = 0 To cblCalcType.Items.Count - 1
            If cblCalcType.Items(i).Selected Then
                selChkbox += cblCalcType.Items(i).Value
            End If
        Next

        If selChkbox <> "" Then
            mySelectQuery = ""
            If selChkbox Like "*N*" Then
                mySelectQuery = "select b.asset_type as Type,b.product_code,'N' dp_src" & _
                    ",sum(decode(a.month||a.year,'" & mmyyyy & "',a.amount,0)) as mtd_depre" & _
                    ",sum(decode(a.month||a.year,'04'||'" & lblyear.Text & "',a.amount,DECODE(a.month||a.year,'03'||'" & lblyear.Text + 1 & "',A.AMOUNT,0))) as ytd_depre from cfa_depre a" & _
                    ", cfa_asset b where  b.ccn = '" & lblCCN.Text & "' and a.ASSET_NO = b.asset_no and b.asof_date=to_date('" & Me.lblAsOfDate.Text & "','dd/mm/yyyy')" & _
                    " AND b.status ='A' group by b.ccn,b.product_code,b.asset_type"
            End If
       
            'Response.Write(mySelectQuery)
            'Response.End()

            If selChkbox Like "*S*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                mySelectQuery = mySelectQuery & "select b.asset_type as Type,b.product_code,'S' dp_src" & _
                    ",sum(decode(a.month||a.year,'" & mmyyyy & "',a.amount,0)) as mtd_depre" & _
                    ",sum(decode(a.month||a.year,'" & Mid(lblAsOfDate.Text, 4, 2) & "'||'" & lblyear.Text & "',0,a.amount)) as ytd_depre from cfa_depre a" & _
                    ", cfa_asset b where and a.ccn = '" & lblCCN.Text & "' and  a.ASSET_NO = b.asset_no " & _
                    " AND b.status ='X' " & _
                    " and a.asset_no in (select asset_no from cfa_movement m where and ccn ='" & lblCCN.Text & "' and m.asof_date=to_date('" & Me.lblAsOfDate.Text & "','dd/mm/yyyy') and movement_code='SL')" & _
                    " group by b.ccn,b.product_code,b.asset_type"
            End If
            If selChkbox Like "*T*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                mySelectQuery = mySelectQuery & "select a.asset_type as Type,a.product_code,'T' dp_src,b.mtd_depre,b.ytd_depre" & _
" from cfa_asset a ,(select d.ccn,d.asset_no" & _
",sum(decode(d.month||d.year,'" & mmyyyy & "',d.amount,0)) as mtd_depre" & _
",sum(case when d.month||d.year ='" & Mid(lblAsOfDate.Text, 4, 2) & "'||'" & lblyear.Text & "' then d.amount else 0 end) ytd_depre  from cfa_depre d " & _
" where d.ccn='" & lblCCN.Text & "'  group by d.ccn,d.asset_no) b " & _
",(select distinct asof_date,asset_no from cfa_movement where new_ccn='" & lblCCN.Text & "' and movement_code='TRAN'" & _
" and asof_date=to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy')) c" & _
" where(a.asset_no = b.asset_no And a.asset_no = c.asset_no)" & _
"and a.asof_date=to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy') and a.ccn='" & lblCCN.Text & "'  "
            End If
            If selChkbox Like "*E*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                mySelectQuery = mySelectQuery & "select a.asset_type as Type,a.product_code,'E' dp_src,b.mtd_depre,b.ytd_depre" & _
" from (select *  from cfa_asset  where ccn ='" & lblCCN.Text & "' and status ='A') a ,(select d.ccn,d.asset_no" & _
",sum(decode(d.month||d.year,'" & mmyyyy & "',d.amount,0)) as mtd_depre" & _
",sum(case when d.month||d.year ='" & Mid(lblAsOfDate.Text, 4, 2) & "'||'" & lblyear.Text & "' then d.amount else 0 end) ytd_depre  from cfa_depre d " & _
" where d.ccn='" & lblCCN.Text & "'  group by d.ccn,d.asset_no) b " & _
",(select distinct asset_no from cfa_addcost where ccn='" & lblCCN.Text & "' and asof_date=to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy') and expense_status='N') c" & _
" where(a.asset_no = b.asset_no  And a.asset_no = c.asset_no) /* and a.asof_date=to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy') */   "
            End If

            If selChkbox Like "*P*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                mySelectQuery = mySelectQuery & "select b.asset_type as Type,b.product_code,'P' dp_src" & _
                    ",sum(decode(a.month||a.year,'" & mmyyyy & "',a.amount,0)) as mtd_depre" & _
                    ",sum(decode(a.month||a.year,'" & Mid(lblAsOfDate.Text, 4, 2) & "'||'" & lblyear.Text & "',0,a.amount)) as ytd_depre from cfa_depre a" & _
                    ", cfa_asset b where a.ASSET_NO = b.asset_no and b.ccn = '" & lblCCN.Text & "'" & _
                    " AND b.ESTIMATE_EXPENSE is not null and b.PROVISION_DATE is not null" & _
                    " group by b.ccn,b.product_code,b.asset_type"
            End If

            myOrderQuery = "SELECT * FROM (" & mySelectQuery & ") "
            'Response.Write(myOrderQuery)
            'Response.End()

            'order by a.ccn asc,a.product_code asc,a.asset_type asc ,a.old_asset_no asc 
            Dim dt As DataTable = DBcon.GetDataTable(myOrderQuery)
            gvDepre_TypeSub.DataSource = dt
            gvDepre_TypeSub.DataBind()

        Else
            MessageBox("please select type of process")
        End If

    End Sub

    Sub showAsset()

       Dim DBcon As New DbConnect(Session("dbsrcName"))

        Dim selChkbox As String = [String].Empty
        For i As Integer = 0 To cblCalcType.Items.Count - 1
            If cblCalcType.Items(i).Selected Then
                selChkbox += cblCalcType.Items(i).Value
            End If
        Next
        'Response.Write(selChkbox)

        If selChkbox <> "" Then
            
            mySelectQuery = ""
            myOrderQuery = ""
            If selChkbox Like "*N*" Then
                mySelectQuery = "(select 'NEW' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date ,inv_qty,depre_method,0 as accu,(nvl(a.acquisition_cost,0)-0) as netbv,nvl((select sum(decode(expense_type,'999',0,expense_amount_thb)) expense_amount_thb from cfa_addcost where asset_no = a.asset_no /*and expense_status='N'*/ and asof_date<=to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy') ),0) adj_cost from cfa_asset A where ccn  = '" & lblCCN.Text & "' and status = 'N' ) "
            End If
            If selChkbox Like "*R*" Then
                mySelectQuery = "(select 'RECALC' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date ,inv_qty,depre_method,0 as accu,(nvl(a.acquisition_cost,0)-0) as netbv,0 adj_cost from cfa_asset A where ccn  = '" & lblCCN.Text & "' and status = 'A' and asset_no like  nvl('%" & txtselasset.Text.Trim & "%',asset_no) and   start_date <=   to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy')  and  end_date >= trunc(to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy'),'mm') ) "
            End If
            If selChkbox Like "*S*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                'mySelectQuery = mySelectQuery & " (select  'SALE' AS TYPE,A.*,b.accu,(a.acquisition_cost-b.accu) as netbv from cfa_asset A,(select asset_no,sum(amount) as accu from cfa_depre where ccn='" & Me.lblCCN.Text & "' group by asset_no) b where a.asset_no=b.asset_no and ccn  = '" & lblCCN.Text & "' and status = 'A'  AND a.ASSET_NO IN (SELECT ASSET_NO FROM CFA_MOVEMENT WHERE MOVEMENT_CODE like 'SL%' and movement_Status ='N' ))"
                mySelectQuery = mySelectQuery & " (select  'SALE' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date,inv_qty,depre_method,nvl(b.accu,0) accu,(nvl(a.acquisition_cost,0)-nvl(b.accu,0)) as netbv,0 adj_cost from cfa_asset A,(select asset_no,nvl(sum(amount),'0')as accu" & _
        " from cfa_depre where to_char(year||month) <= to_char(substr('" & Me.lblAsOfDate.Text & "',7,4)||substr('" & Me.lblAsOfDate.Text & "',4,2))" & _
"  and ASSET_NO IN (SELECT ASSET_NO FROM CFA_MOVEMENT WHERE   MOVEMENT_CODE like 'SL%' and movement_Status ='N') group by asset_no) b where a.ccn ='" & lblCCN.Text & "' and  a.asset_no=b.asset_no  and status = 'A'  /*AND a.ASSET_NO IN (SELECT ASSET_NO FROM CFA_MOVEMENT WHERE MOVEMENT_CODE like 'SL%' and movement_Status ='N' )*/ )"
            End If
            If selChkbox Like "*T*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                'mySelectQuery = mySelectQuery & " (select  'TRAN' AS TYPE,A.*,b.accu,(a.acquisition_cost-b.accu) as netbv from cfa_asset A,(select asset_no,sum(amount) as accu from cfa_depre where ccn='" & Me.lblCCN.Text & "' group by asset_no) b where a.asset_no=b.asset_no and ccn  = '" & lblCCN.Text & "' and status = 'A' AND a.ASSET_NO IN (SELECT ASSET_NO FROM CFA_MOVEMENT WHERE MOVEMENT_CODE like 'TR%' and movement_Status ='A' ))"
                mySelectQuery = mySelectQuery & " (select  'TRAN' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date,inv_qty,depre_method,b.accu,(nvl(a.acquisition_cost,0)-nvl(b.accu,0)) as netbv,0 adj_cost from cfa_asset A" & _
                ",(select asset_no,nvl(sum(amount),'0')as accu" & _
        " from cfa_depre where to_char(year||month) <= to_char(substr('" & Me.lblAsOfDate.Text & "',7,4)||substr('" & Me.lblAsOfDate.Text & "',4,2)) and ccn='" & lblCCN.Text & "' and ASSET_NO IN (SELECT ASSET_NO FROM CFA_MOVEMENT WHERE MOVEMENT_CODE like 'TR%' and movement_Status ='A') group by asset_no) b where ccn  = '" & lblCCN.Text & "' and  a.asset_no=b.asset_no and status = 'A' AND a.ASSET_NO IN (SELECT ASSET_NO FROM CFA_MOVEMENT WHERE MOVEMENT_CODE like 'TR%' and movement_Status ='A' ))"
            End If
            If selChkbox Like "*E*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                mySelectQuery = mySelectQuery & " (select  'EXP' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date,inv_qty,depre_method,nvl(b.accu,0) accu,nvl(a.acquisition_cost,0) as netbv,0 adj_cost from cfa_asset A ,(select asset_no,nvl(sum(amount),0) as accu from cfa_depre where ccn='" & Me.lblCCN.Text & "' group by asset_no) b where a.asset_no=b.asset_no(+) and a.ccn  = '" & lblCCN.Text & "' and status in ('A') AND a.ASSET_NO IN (SELECT ASSET_NO FROM CFA_ADDCOST WHERE asof_date = to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy') and expense_Status in ('N','A','P') ))"
                ' mySelectQuery = mySelectQuery & " (select  'EXP' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date,inv_qty,depre_method,b.accu,(nvl(a.acquisition_cost,0)/*-nvl(b.accu,0) */) as netbv from cfa_asset A ,(select asset_no,nvl(sum(amount),'0')as accu" & _
                ' " from cfa_depre where to_char(year||month) <= to_char(substr('" & Me.lblAsOfDate.Text & "',7,4)||substr('" & Me.lblAsOfDate.Text & "',4,2)) and ccn='" & lblCCN.Text & "'  /*and ASSET_NO IN (SELECT ASSET_NO FROM CFA_ADDCOST WHERE asof_date = to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy') and expense_Status ='N')*/ group by asset_no) b where a.asset_no=b.asset_no and ccn  = '" & lblCCN.Text & "' and status =  'A' AND a.ASSET_NO IN (SELECT ASSET_NO FROM CFA_ADDCOST WHERE asof_date = to_date('" & lblAsOfDate.Text & "','dd/mm/yyyy') and expense_Status ='N' ))"
            End If
            'Response.Write(mySelectQuery)
            'Response.End()

            If selChkbox Like "*P*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                mySelectQuery = mySelectQuery & " (select  'EST' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date,inv_qty,depre_method,b.accu,(nvl(a.acquisition_cost,0)-nvl(b.accu,0)) as netbv ,0 adj_cost from cfa_asset A" & _
                ",(select asset_no,nvl(sum(amount),'0')as accu" & _
        " from cfa_depre where to_char(year||month) <= to_char(substr('" & Me.lblAsOfDate.Text & "',7,4)||substr('" & Me.lblAsOfDate.Text & "',4,2)) and ccn='" & lblCCN.Text & "'  group by asset_no) b where a.asset_no=b.asset_no and ccn  = '" & lblCCN.Text & "' and status = 'A' AND a.ESTIMATE_EXPENSE is not null and a.PROVISION_DATE is not null)"
            End If

            If selChkbox Like "*M*" Then
                If mySelectQuery <> "" Then
                    mySelectQuery = mySelectQuery & " union "
                End If
                mySelectQuery = mySelectQuery & "(select 'M/T' AS TYPE,ccn,a.asset_no,old_asset_no,asset_description,asset_type,product_code,age,to_char(start_date,'dd/mm/yyyy') start_date,to_char(end_date,'dd/mm/yyyy') end_date,inv_qty,depre_method,nvl(b.accu,0),(nvl(a.acquisition_cost,0)-nvl(b.accu,0)) as netbv,0 adj_cost from cfa_asset A" & _
                " ,(select asset_no,nvl(sum(amount),'0')as accu" & _
        " from cfa_depre where to_char(year||month) <= to_char(substr('" & Me.lblAsOfDate.Text & "',7,4)||substr('" & Me.lblAsOfDate.Text & "',4,2)) and ccn='" & lblCCN.Text & "'  group by asset_no) b where a.asset_no=b.asset_no and ccn  = '" & lblCCN.Text & "' and status = 'M')"
            End If
            'Response.Write(mySelectQuery)
            'Response.End()
            myOrderQuery = "SELECT * FROM (" & mySelectQuery & ")  order by start_date,ccn asc,product_code asc,asset_type asc ,old_asset_no asc "



             Dim dt As DataTable = DBcon.GetDataTable(myOrderQuery)
            gvAsset.DataSource = dt
            gvAsset.DataBind()
        Else
            MessageBox("please select type of process")
        End If

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Me.gvAsset.Rows.Count > 0 Then
            gvAsset.DataSource = Nothing
            gvAsset.DataBind()
        ElseIf Me.gvDepre_TypeSub.Rows.Count > 0 Then
            gvDepre_TypeSub.DataSource = Nothing
            gvDepre_TypeSub.DataBind()
        Else
            Response.Redirect("main_page.aspx")
        End If

    End Sub

    Protected Sub btnCalc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalc.Click
        If Session("userid") <> "" Then
            Dim selChkbox As String = [String].Empty
            For i As Integer = 0 To cblCalcType.Items.Count - 1
                If cblCalcType.Items(i).Selected Then
                    selChkbox += cblCalcType.Items(i).Value
                End If
            Next
            Dim errFlag As String = " "
            Dim cnt As Integer = 0
            For Each gvr As GridViewRow In gvAsset.Rows
                Dim cb As CheckBox = CType(gvr.FindControl("cbCalc"), CheckBox)

                If cb.Checked = True Then
                    cnt += 1
                    'Response.Write(gvr.Cells(2).Text)
                    Dim DBCon As New DbConnect(Session("dbSrcName"))
                    Dim cmd As Data.OracleClient.OracleCommand = DBCon.CreateCommand("TYG_FA_DP_CALC")
                    Dim assetno As Label = CType(gvr.FindControl("lblassetno"), Label)
                    cmd.CommandType = Data.CommandType.StoredProcedure
                    cmd.Parameters.Add(New System.Data.OracleClient.OracleParameter("p_ccn", _
                    System.Data.OracleClient.OracleType.VarChar)).Value = lblCCN.Text
                    cmd.Parameters.Add(New System.Data.OracleClient.OracleParameter("p_asof_date", _
                    System.Data.OracleClient.OracleType.VarChar)).Value = lblAsOfDate.Text
                    cmd.Parameters.Add(New System.Data.OracleClient.OracleParameter("p_type", _
                    System.Data.OracleClient.OracleType.VarChar)).Value = selChkbox   ' possible value 'NSTER'
                    ' Response.Write(selChkbox)
                    cmd.Parameters.Add(New System.Data.OracleClient.OracleParameter("p_doc", _
                    System.Data.OracleClient.OracleType.VarChar)).Value = assetno.Text ' gvr.Cells(2).Text
                    'Response.Write(gvr.Cells(2).Text)
                    cmd.Parameters.Add(New System.Data.OracleClient.OracleParameter("P_user", _
                    System.Data.OracleClient.OracleType.VarChar)).Value = Session("userid")
                    '  Response.Write(cmd.Parameters.Item(0).Value & " - > " & cmd.Parameters.Item(1).Value & "->" & cmd.Parameters.Item(2).Value & "->" & cmd.Parameters.Item(3).Value & "->" & cmd.Parameters.Item(4).Value)
                    DBCon.Execute(cmd)
                    If Err.Number < 0 Then
                        MessageBox("Can not calculate depreciation ...Contact system incharged !!!")
                        errFlag = "E"
                        Exit For
                    Else
                    End If
                End If
            Next
            'Response.End()
            If cnt <> 0 Then
                If errFlag = " " Then
                    MessageBox("Depreciation Calculation completed...")
                    showAsset()
                End If
            Else
                MessageBox("No record selected for calculation !!!")
            End If


        End If
        
    End Sub


    Dim AccuTotal As Decimal = 0
    Dim Accuadjcost As Decimal = 0

    Protected Sub gvAsset_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAsset.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' add the UnitPrice and QuantityTotal to the running total variables
            AccuTotal += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetBV"))

            Dim datatype As Label = CType(e.Row.FindControl("lbldatatype"), Label)
            Dim assetno As Label = CType(e.Row.FindControl("lblassetno"), Label)
            Dim adjcost As Label = CType(e.Row.FindControl("lbladjcost"), Label)
            Accuadjcost += CDec(adjcost.Text)
             If datatype.Text.Trim <> "NEW" Then
                adjcost.Text = FormatNumber(getadjcost(assetno.Text, datatype.Text))
            End If
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(9).Text = "Totals:"
            ' for the Footer, display the running totals
            e.Row.Cells(10).Text = AccuTotal.ToString("N2")
            e.Row.Cells(10).HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True
            e.Row.Cells(14).Text = Accuadjcost.ToString("N2")
            e.Row.Cells(14).HorizontalAlign = HorizontalAlign.Right

        End If

    End Sub
    Function getadjcost(ByVal inasset As String, ByVal indatatype As String) As Decimal
        Dim adjcost As Decimal = 0.0
        Dim DBcon As New DbConnect(Session("dbsrcName"))
        Dim sql As String = ""
        If indatatype = "EXP" Then
            sql = "select nvl(sum(decode(expense_type,'999',0,expense_amount_thb)),0) amount  from cfa_addcost where asset_no ='" & inasset & "' and expense_status ='N'  "
        ElseIf indatatype = "SALE" Then
            sql = "select nvl(sum(new_book_value),0) amount  from cfa_movement where asset_no ='" & inasset & "' and movement_code ='SL' and movement_status ='N'  "
        Else
            sql = "select 0 amount from dual"
        End If
        'Response.Write(sql)
        'Response.End()

        For Each dr As DataRow In DBcon.GetDataTable(sql).Rows
            adjcost = dr("amount")
        Next
        Return adjcost
    End Function

    Dim mth_depre As Decimal = 0
    Dim ytd_depre As Decimal = 0
    Protected Sub gvDepre_TypeSub_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDepre_TypeSub.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            mth_depre += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "mtd_depre"))
            ytd_depre += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ytd_depre"))
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(1).Text = "Totals:"

            e.Row.Cells(2).Text = mth_depre.ToString("N2")
            e.Row.Cells(2).HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True

            e.Row.Cells(3).Text = ytd_depre.ToString("N2")
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True
        End If
    End Sub

    Private Sub MessageBox(ByVal msg As String)
        ' define a javascript alertbox containing the string passed in as argument
        ' create a new label
        Dim lbl As New Label()
        ' add the javascript to fire an alertbox to the label and
        ' add the string argument passed to the subroutine as the
        ' message payload for the alertbox
        lbl.Text = "<script language='javascript'>" & Environment.NewLine & "window.alert('" + msg + "')</script>"
        ' add the label to the page to display the alertbox
        Page.Controls.Add(lbl)
    End Sub

    Protected Sub ddl_ccn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_ccn.SelectedIndexChanged
        lblCCN.Text = ddl_ccn.SelectedValue
        CommonFN.GetAsofdate(ddl_ccn, lblAsOfDate, lblyear, Session())
        txtasofdate.Text = lblAsOfDate.Text
        txtasofdate.Visible = True
        lblddmmyy_txt.Visible = True
        gvAsset.DataSource = Nothing
        gvAsset.DataBind()
    End Sub

    Protected Sub cbSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each gvr As GridViewRow In gvAsset.Rows
            Dim Hchk As CheckBox = CType(gvAsset.HeaderRow.FindControl("cbSelAll"), CheckBox)
            Dim Dchk As CheckBox = CType(gvr.Cells(0).FindControl("cbCalc"), CheckBox)
            If Hchk.Checked = True Then
                Dchk.Checked = True
            End If

            If Hchk.Checked = False Then
                Dchk.Checked = False
            End If

            If Dchk.Enabled = False Then
                Dchk.Checked = False
            End If

        Next
    End Sub

    Protected Sub cblCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblCalcType.SelectedIndexChanged
        Me.gvAsset.DataSource = Nothing
        gvAsset.DataBind()
        Me.gvDepre_TypeSub.DataSource = Nothing
        Me.gvDepre_TypeSub.DataBind()
        If Left(cblCalcType.SelectedValue, 1) = "R" Then
            txtselasset.Visible = True
        Else
            txtselasset.Visible = False
        End If
    End Sub

    Protected Sub txtasofdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtasofdate.TextChanged
        '  txtasofdate.Text = CommonFN.DateCheck(txtasofdate.Text)
        'If txtasofdate.Text.ToString = "" Then
        '    MessageBox("Invalid date !!!")
        'Else
        lblAsOfDate.Text = txtasofdate.Text
        'End If
    End Sub
End Class
